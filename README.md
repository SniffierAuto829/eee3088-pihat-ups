# EEE3088 PiHat UPS
The PiHAt is intended to be used as either an uninterrupted power supply allowing the user to safely turn off the

Pi should it suddenly use power. The PiHat can also be used as a portable battery pack allowing the Pi to be used 

even it isn't connected to mains power. The PiHat is designed to power the Pi for a maximum of 2 hours. The batteries

can however be quickly swapped by the user.

The PiHat follows the micro PiHat standard layed out in the microHat README.md
